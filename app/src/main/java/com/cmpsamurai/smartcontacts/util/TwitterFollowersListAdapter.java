package com.cmpsamurai.smartcontacts.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.TwitterFollower;

import java.util.ArrayList;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class TwitterFollowersListAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    ArrayList<TwitterFollower> twitterFollowers=null;


    public TwitterFollowersListAdapter(FragmentActivity activity, ArrayList<TwitterFollower> followers) {
        context=activity;
        this.twitterFollowers=followers;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return twitterFollowers.size();
    }

    @Override
    public TwitterFollower getItem(int position) {
        return twitterFollowers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        rowView = inflater.inflate(R.layout.twitter_contact_text_view, null);

        Holder holder=new Holder();
       holder.tv=(TextView) rowView.findViewById(R.id.twitterContactName);
        holder.img=(ImageView) rowView.findViewById(R.id.twitterContactImage);
        holder.tv.setText(twitterFollowers.get(position).getName());
        ImageDownloadTask task = new ImageDownloadTask(holder.img);
        task.execute(twitterFollowers.get(position).getPicurl());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + twitterFollowers.get(position).getUserScreen())));
                }catch (Exception e) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + twitterFollowers.get(position).getUserScreen())));
                }
            }
        });

        return rowView;
    }




}
