package com.cmpsamurai.smartcontacts.util;

import android.support.v4.app.FragmentActivity;

import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.ui.MainActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class FacebookFriends {
    private static FacebookContactListAdapter adapter;

    public static FacebookContactListAdapter getAdapter(FragmentActivity activity)
    {
        if(adapter ==null)
        {
            adapter = new FacebookContactListAdapter(activity,FacebookFriends);
        }
        return adapter;
    }

    public static ArrayList<FacebookFriend> getFacebookFriends() {
        return FacebookFriends;
    }

    public static ArrayList<FacebookFriend> FacebookFriends= new ArrayList<>();

    public static void refreshFriendList()
    {
        if(adapter!=null)
            adapter.notifyDataSetInvalidated();
        GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONArray rawData = new JSONArray();
                            rawData = response.getJSONObject().getJSONArray("data");
                            FacebookFriends.clear();
                            for (int l = 0; l < rawData.length(); l++) {
                                FacebookFriend friend = new FacebookFriend();
                                friend.setId(rawData.getJSONObject(l).getString("id"));
                                friend.setName(rawData.getJSONObject(l).getString("name"));
                                FacebookFriends.add(friend);
                                if(adapter!=null)
                                    adapter.notifyDataSetChanged();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }


}
