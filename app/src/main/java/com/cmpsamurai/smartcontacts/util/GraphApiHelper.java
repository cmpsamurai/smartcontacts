package com.cmpsamurai.smartcontacts.util;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Lenovo-Laptop on 5/20/2016.
 */
public class GraphApiHelper {
    /**
     *
     * @param requestString
     */
    public static void MakePaginatedRequest(String requestString)
    {
        final ArrayList<JSONObject> results=new ArrayList<JSONObject>();
        final String[] afterString = {""};  // will contain the next page cursor
        final Boolean[] noData = {false};   // stop when there is no after cursor
        do {
            Bundle params = new Bundle();
            params.putString("after", afterString[0]);
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    requestString,
                    params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse graphResponse) {
                            JSONObject jsonObject = graphResponse.getJSONObject();
                            try {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    results.add(jsonArray.getJSONObject(i));
                                }
                                if(!jsonObject.isNull("paging")) {
                                    JSONObject paging = jsonObject.getJSONObject("paging");
                                    JSONObject cursors = paging.getJSONObject("cursors");
                                    if (!cursors.isNull("after"))
                                        afterString[0] = cursors.getString("after");
                                    else
                                        noData[0] = true;
                                }
                                else
                                    noData[0] = true;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAndWait();
        }
        while(!noData[0] == true);
    }


}
