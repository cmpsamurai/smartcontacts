package com.cmpsamurai.smartcontacts.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Lenovo-Laptop on 5/27/2016.
 */
public class ImageDownloadTask extends AsyncTask<String, Integer, Bitmap> {
    private ImageView mView;
    private static HashMap<String,Bitmap> image_cache = new HashMap<>();

    public ImageDownloadTask(ImageView view){
        mView = view;
    }
    @Override
    protected Bitmap doInBackground(String... params) {
        String url =params[0];
        Bitmap bitmap=null;
        if(image_cache.containsKey(url))
            return image_cache.get(url);
        try {
            URL imageURL = new URL(url);
            bitmap= BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            image_cache.put(url,bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


    @Override
    protected void onPostExecute(Bitmap result) {
        mView.setImageBitmap(result);
    }
}
