package com.cmpsamurai.smartcontacts.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.ui.FacebookContactActivity;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class FacebookContactListAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    ArrayList<FacebookFriend> fbFriends=null;
    public FacebookContactListAdapter(FragmentActivity activity, ArrayList<FacebookFriend> fbFriends) {
        context=activity;
        this.fbFriends=fbFriends;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return fbFriends.size();
    }

    @Override
    public FacebookFriend getItem(int position) {
        return fbFriends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class Holder
    {
        TextView tv;
        ImageView img;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        Holder holder=new Holder();
        rowView = inflater.inflate(R.layout.facebook_contact_text_view, null);
        holder.tv=(TextView) rowView.findViewById(R.id.fbContactName);
        holder.img=(ImageView) rowView.findViewById(R.id.fbContactImage);
        holder.tv.setText(fbFriends.get(position).getName());
        ImageDownloadTask task = new ImageDownloadTask(holder.img);
        task.execute(fbFriends.get(position).getProfilePictureUrl());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Otherwise single pane layout, start a new ContactDetailActivity with
                // the contact Uri
                Intent intent = new Intent(context, FacebookContactActivity.class);
                intent.putExtra("FACEBOOK_CONTACT",fbFriends.get(position));
                context.startActivity(intent);
            }
        });
        return rowView;
    }




}
