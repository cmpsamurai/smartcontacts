package com.cmpsamurai.smartcontacts.util;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

public class MyTwitterApiClient extends TwitterApiClient {
    public MyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public FollowersService getFollowersService() {
        return getService(FollowersService.class);
    }
}

// example users/show service endpoint
interface FollowersService {
    // Cant Handle Cursors
    @GET("/1.1/friends/list.json")
    void list_followers(@Query("user_id") long id,@Query("cursor") long cursor, Callback<Response> cb);
}