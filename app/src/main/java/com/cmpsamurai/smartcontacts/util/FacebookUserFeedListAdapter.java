package com.cmpsamurai.smartcontacts.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.models.FacebookPost;
import com.cmpsamurai.smartcontacts.ui.FacebookContactActivity;
import com.cmpsamurai.smartcontacts.ui.FacebookPostActivity;

import java.util.ArrayList;

/**
 * Created by mahmoudhosny on 5/20/16.
 */
public class FacebookUserFeedListAdapter extends BaseAdapter{
    Context context;
    private ArrayList<FacebookPost> fbPosts = null;
    private static LayoutInflater inflater = null;

    TextView datetv;
    TextView timetv;
    TextView storytv;
    ImageView storyUserIv;

    FacebookUserFeedListAdapter(FragmentActivity activity, ArrayList<FacebookPost> posts){
        this.fbPosts = posts;
        context = activity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return fbPosts.size();
    }

    @Override
    public FacebookPost getItem(int position) {
        return fbPosts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView ;
        rowView = inflater.inflate(R.layout.facebook_user_posts_detail, null);
        datetv = (TextView) rowView.findViewById(R.id.post_date);
        timetv = (TextView) rowView.findViewById(R.id.post_time);
        storytv = (TextView) rowView.findViewById(R.id.post_story);
        storyUserIv =(ImageView)rowView.findViewById(R.id.fbPostContactImage);
        ImageDownloadTask task = new ImageDownloadTask(storyUserIv);
        FacebookFriend ff;
        ff=((FacebookContactActivity)context).getFacebookFriend();
        task.execute(ff.getProfilePictureUrl());


        datetv.setText(fbPosts.get(position).getCreated_time().substring(0,10));
        timetv.setText(fbPosts.get(position).getCreated_time().substring(11,11+8));
        storytv.setText(fbPosts.get(position).getStory());

        rowView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String post_id=fbPosts.get(position).getId();
                        String uri = "fb://post/"+post_id;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        context.startActivity(intent);
                    }
                }
        );

        return rowView;
    }
}
