package com.cmpsamurai.smartcontacts.util;

import android.support.v4.app.FragmentActivity;

import com.cmpsamurai.smartcontacts.models.FacebookPost;

import java.util.ArrayList;

/**
 * Created by mahmoudhosny on 5/20/16.
 */
public class FacebookPosts {

    private static FacebookUserFeedListAdapter adapter;

    public static FacebookUserFeedListAdapter getAdapter(FragmentActivity activity)
    {
        if(adapter ==null)
        {
            adapter = new FacebookUserFeedListAdapter(activity,fbPosts);
        }
        return adapter;
    }

    public static ArrayList<FacebookPost> getFacebookPosts() {
        return fbPosts;
    }

    public static ArrayList<FacebookPost> fbPosts= new ArrayList<>();

    public static void setFacebookPosts(ArrayList<FacebookPost> posts){
        fbPosts = posts;
    }

}
