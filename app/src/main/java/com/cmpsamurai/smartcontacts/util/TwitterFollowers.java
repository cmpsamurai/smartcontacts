package com.cmpsamurai.smartcontacts.util;

import android.support.v4.app.FragmentActivity;

import com.cmpsamurai.smartcontacts.models.TwitterFollower;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.client.Response;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class TwitterFollowers {
    private static TwitterFollowersListAdapter adapter;

    public static TwitterFollowersListAdapter getAdapter(FragmentActivity activity)
    {
        if(adapter ==null)
        {
            adapter = new TwitterFollowersListAdapter(activity,TwitterFollowers);
        }
        return adapter;
    }

    private static long cursor = -1;
    public static ArrayList<TwitterFollower> getTwitterFollowers() {
        return TwitterFollowers;
    }

    public static ArrayList<TwitterFollower> TwitterFollowers= new ArrayList<>();

    public static void refreshFollowersList()
    {
        if(adapter!=null)
            adapter.notifyDataSetInvalidated();
        TwitterSession session = Twitter.getInstance().getSessionManager().getActiveSession();
        if(session ==null) return;

        getTwitterFollowers().clear();
        MyTwitterApiClient apiclients=new MyTwitterApiClient(session);

        apiclients.getFollowersService().list_followers(session.getUserId(), com.cmpsamurai.smartcontacts.util.TwitterFollowers.cursor, new Callback<Response>() {

            @Override
            public void failure(TwitterException arg0) {
                // TODO Auto-generated method stub
                arg0.printStackTrace();
            }

            @Override
            public void success(Result<Response> arg0) {
                // TODO Auto-generated method stub
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {

                    reader = new BufferedReader(new InputStreamReader(arg0.response.getBody().in()));

                    String line;

                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                String result = sb.toString();
                System.out.println("Response is>>>>>>>>>"+result);
                try {
                    JSONObject obj=new JSONObject(result);
                    com.cmpsamurai.smartcontacts.util.TwitterFollowers.cursor = obj.getLong("next_cursor");
                    JSONArray users=obj.getJSONArray("users");
                    //This is where we get ids of followers
                    for(int i=0;i<users.length();i++){
                        System.out.println("Id of user "+(i+1)+" is "+users.get(i));
                        com.cmpsamurai.smartcontacts.util.TwitterFollowers.getTwitterFollowers().add(new TwitterFollower(
                                users.getJSONObject(i).getString("name"),
                                users.getJSONObject(i).getString("id"),
                                users.getJSONObject(i).getString("screen_name"),
                                users.getJSONObject(i).getString("profile_image_url")
                        ));
                    }
                    TwitterSession s = Twitter.getInstance().getSessionManager().getActiveSession();
                    MyTwitterApiClient a = new MyTwitterApiClient(s);
                    if (com.cmpsamurai.smartcontacts.util.TwitterFollowers.cursor!=0)
                    a.getFollowersService().list_followers(s.getUserId(), com.cmpsamurai.smartcontacts.util.TwitterFollowers.cursor,this);
                    if(adapter!=null)
                        adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        });
    }


}
