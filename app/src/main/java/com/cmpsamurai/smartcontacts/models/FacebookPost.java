package com.cmpsamurai.smartcontacts.models;

import java.io.Serializable;

/**
 * Created by mahmoudhosny on 5/20/16.
 */
public class FacebookPost implements Serializable{
    private String id;
    private String created_time;
    private String story;

    public FacebookPost(){

    }

    public FacebookPost(String created_time, String id, String story) {
        this.created_time = created_time;
        this.id = id;
        this.story = story;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    @Override
    public String toString() {
        return "id "+ getId() + "\ncreated_time " + getCreated_time()+"\nstory " + getStory();
    }
}
