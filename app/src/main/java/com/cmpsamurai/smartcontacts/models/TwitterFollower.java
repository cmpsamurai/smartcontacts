package com.cmpsamurai.smartcontacts.models;

/**
 * Created by Lenovo-Laptop on 5/27/2016.
 */
public class TwitterFollower {
    private String name;
    private String id;
    private String userScreen;
    private String picurl;

    public TwitterFollower(String name, String id, String userScreen, String picurl) {
        this.name = name;
        this.id = id;
        this.userScreen = userScreen;
        this.picurl = picurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserScreen() {
        return userScreen;
    }

    public void setUserScreen(String userScreen) {
        this.userScreen = userScreen;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }
}
