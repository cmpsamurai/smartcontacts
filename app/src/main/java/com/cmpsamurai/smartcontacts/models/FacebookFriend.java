package com.cmpsamurai.smartcontacts.models;

import java.io.Serializable;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class FacebookFriend  implements Serializable {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePictureUrl()
    {
        return "https://graph.facebook.com/" + id + "/picture?type=large";
    }
/*
    public Bitmap getFacebookProfilePicture(){
        Bitmap bitmap=null;
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }
*/

    private String name;
    private String id;
}
