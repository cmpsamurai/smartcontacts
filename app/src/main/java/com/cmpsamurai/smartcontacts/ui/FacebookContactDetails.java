package com.cmpsamurai.smartcontacts.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.models.FacebookPost;
import com.cmpsamurai.smartcontacts.util.FacebookPosts;
import com.cmpsamurai.smartcontacts.util.ImageDownloadTask;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FacebookContactDetails extends Fragment {

    private static FacebookFriend facebookFriend;
    public FacebookContactDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FacebookContactDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static FacebookContactDetails newInstance(FacebookFriend ff) {
        facebookFriend = ff;
        FacebookContactDetails fragment = new FacebookContactDetails();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.facebook_user_basic_info, container, false);
        TextView ffName = (TextView) view.findViewById(R.id.facebook_contact_name);
        ImageView ffImage =  (ImageView) view.findViewById(R.id.facebook_contact_image);
        ffName.setText(facebookFriend.getName());

        ImageDownloadTask task = new ImageDownloadTask(ffImage);
        task.execute(facebookFriend.getProfilePictureUrl());


        return view;
    }


}
