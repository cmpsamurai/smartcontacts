package com.cmpsamurai.smartcontacts.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.facebook.AccessToken;
import com.twitter.sdk.android.Twitter;

/**
 * Created by Lenovo-Laptop on 4/22/2016.
 */
public class MainPagerAdapter  extends FragmentStatePagerAdapter {
    public final String main_tab_title="Phone Contacts";
    public final String favorites_tab_title="Favorites";
    public final String facebook_tab_title="Facebook Contacts";
    public final String twitter_tab_title="Twitter Contacts";

    ContactsListFragment contacts_list_fragment;
    TwitterFragment twitter_fragment;
    TwitterLoggedInFragment twitter_logged_in_fragment;


    public boolean isFacebookLoggedIn() {
        return facebookLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.facebookLoggedIn = loggedIn;
    }

    private boolean facebookLoggedIn = AccessToken.getCurrentAccessToken()!=null;

    public boolean isTwitterLoggedIn() {
        return twitterLoggedIn;
    }

    private boolean twitterLoggedIn = Twitter.getInstance().getSessionManager().getActiveSession()!=null;


    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        twitter_fragment=TwitterFragment.newInstance();
        contacts_list_fragment = ContactsListFragment.newInstance();
        twitter_logged_in_fragment=TwitterLoggedInFragment.newInstance();
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0 : return contacts_list_fragment;
            case 1 :
                if(!facebookLoggedIn)
                    return FacebookFragment.newInstance();
                else
                    return FacebookLoggedIn.newInstance();
            case 2 :
                    if(!twitterLoggedIn)
                        return twitter_fragment;
                    else
                        return twitter_logged_in_fragment;

            case 3 : return FavoritesFragment.newInstance();
            default : return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position)
        {
            case 0 : return main_tab_title;
            case 1 : return facebook_tab_title;
            case 2 : return twitter_tab_title;
            case 3 : return favorites_tab_title;
            default: return "Object" + + (position + 1);
        }

    }

    @Override
    public int getItemPosition(Object object)
    {
        if (object instanceof FacebookFragment && isFacebookLoggedIn()) {
            return POSITION_NONE;
        }
        if (object instanceof FacebookLoggedIn &&
                !isFacebookLoggedIn()) {
            return POSITION_NONE;
        }
        return POSITION_UNCHANGED;
    }


}
