package com.cmpsamurai.smartcontacts.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.models.FacebookPost;
import com.cmpsamurai.smartcontacts.util.FacebookPosts;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FacebookContactPosts extends Fragment {

    private JSONArray postsJsonArray;
    private ArrayList<FacebookPost> postsList;
    private static FacebookFriend facebookFriend;
    public FacebookContactPosts() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FacebookContactDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static FacebookContactPosts newInstance(FacebookFriend ff) {
        facebookFriend = ff;
        FacebookContactPosts fragment = new FacebookContactPosts();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.facebook_user_feeds, container, false);


        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + facebookFriend.getId() + "/posts",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        System.out.print(response);
                        String rawResponse = response.getRawResponse();

                        JSONObject obj = response.getJSONObject();
                        try {
                            postsJsonArray = obj.getJSONArray("data");
                            postsList = new ArrayList<FacebookPost>();
                            for(int i=0; i<postsJsonArray.length(); i++){
                                String story = postsJsonArray.getJSONObject(i).has("story")?postsJsonArray.getJSONObject(i).getString("story"):postsJsonArray.getJSONObject(i).getString("message");;
                                postsList.add(new FacebookPost(postsJsonArray.getJSONObject(i).getString("created_time"),
                                        postsJsonArray.getJSONObject(i).getString("id"),
                                        story));

                            }
                                FacebookPosts.setFacebookPosts(postsList);
                            ListView listView = (ListView) view.findViewById(R.id.facebook_user_feed_listView);
                            listView.setAdapter(FacebookPosts.getAdapter(getActivity()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();


        return view;
    }


}
