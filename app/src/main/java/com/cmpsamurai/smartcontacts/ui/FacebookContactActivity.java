package com.cmpsamurai.smartcontacts.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class FacebookContactActivity extends FragmentActivity {
    FacebookFriend facebookFriend;
    FacebookContactPagerAdapter mFacebookContactPagerAdapter;
    ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        // Initialize the SDK before executing any other operations,

        setContentView(R.layout.activity_facebook_contact_detail);
        Intent intent = getIntent();
        facebookFriend = (FacebookFriend)intent.getSerializableExtra("FACEBOOK_CONTACT");

        // fragments, so use getSupportFragmentManager.
        mFacebookContactPagerAdapter = new FacebookContactPagerAdapter(getSupportFragmentManager());
        mFacebookContactPagerAdapter.setFacebookFriend(facebookFriend);
        mViewPager = (ViewPager) findViewById(R.id.facebook_contact_swipe_pager);
        mViewPager.setAdapter(mFacebookContactPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Tapping on top left ActionBar icon navigates "up" to hierarchical parent screen.
                // The parent is defined in the AndroidManifest entry for this activity via the
                // parentActivityName attribute (and via meta-data tag for OS versions before API
                // Level 16). See the "Tasks and Back Stack" guide for more information:
                // http://developer.android.com/guide/components/tasks-and-back-stack.html
                Intent i = new Intent(this, MainActivity.class);
                i.putExtra("TAB","FACEBOOK");
                NavUtils.navigateUpTo(this,i);
                return true;
        }
        // Otherwise, pass the item to the super implementation for handling, as described in the
        // documentation.
        return super.onOptionsItemSelected(item);
    }

    public void viewUserFBFeed(View v)
    {
        String contact_id=facebookFriend.getId();
        String facebookUrl = "https://www.facebook.com/"+contact_id;
        try {
            int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                Uri uri = Uri.parse("fb://profile/"+contact_id);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        } catch (PackageManager.NameNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
    }
    public void viewUserProfile(View v)
    {
        String contact_id=facebookFriend.getId();
        String facebookUrl = "https://www.facebook.com/"+contact_id;
        try {
            int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                Uri uri = Uri.parse("fb://profile/"+contact_id);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        } catch (PackageManager.NameNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
    }


    public FacebookFriend getFacebookFriend()
    {
        return facebookFriend;
    }

}
