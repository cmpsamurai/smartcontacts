package com.cmpsamurai.smartcontacts.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookPost;

/**
 * Created by mahmoudhosny on 5/20/16.
 */
public class FacebookPostActivity extends FragmentActivity {

    FacebookPost post;
    TextView urltv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook_user_specific_post);
        Intent i = getIntent();
        post = (FacebookPost) i.getSerializableExtra("FACEBOOK_POST");
        urltv = (TextView) findViewById(R.id.facebook_post_url);
        urltv.setText(Html.fromHtml("www.facebook.com/"+post.getId()));
        urltv.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
