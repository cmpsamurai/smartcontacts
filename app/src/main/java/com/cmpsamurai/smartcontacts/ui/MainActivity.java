package com.cmpsamurai.smartcontacts.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.util.FacebookFriends;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends FragmentActivity  {
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "d29kppqp6O0ZLaVlmM3V3mKfh";
    private static final String TWITTER_SECRET = "D9gzLJAyIEOQ7qzh0hVI39Az5wML7UjhA46iCjYA55b8KEjMqA";
    Callback<TwitterSession> cbs;

    MainPagerAdapter mMainPagerAdapter;
    ViewPager mViewPager;
    AccessTokenTracker accessTokenTracker;

    // True if this activity instance is a search result view (used on pre-HC devices that load
    // search results in a separate instance of the activity rather than loading results in-line
    // as the query is typed.
    private boolean isSearchResultView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        // Initialize the SDK before executing any other operations,
        // especially, if you're using Facebook UI elements.
        AppEventsLogger.activateApp(this);
        // Set main content view. On smaller screen devices this is a single pane view with one
        // fragment. One larger screen devices this is a two pane view with two fragments.


        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));



        setContentView(R.layout.activity_main);

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                updateWithToken(newAccessToken);
            }
        };

        if(AccessToken.getCurrentAccessToken()!=null)
        {
            FacebookFriends.refreshFriendList();
        }


        // fragments, so use getSupportFragmentManager.
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.main_swipe_pager);
        mViewPager.setAdapter(mMainPagerAdapter);

        Intent i = getIntent();
        if(i!=null )
        {
            String tab= i.getStringExtra("TAB");
            if(tab!=null&&tab.equals("FACEBOOK"))
            mViewPager.setCurrentItem(1);
        }

    }

    private void updateWithToken(AccessToken currentAccessToken) {
            try {
                mMainPagerAdapter.setLoggedIn(currentAccessToken != null && currentAccessToken.getToken() != null);
                mMainPagerAdapter.notifyDataSetChanged();
                mViewPager.setCurrentItem(mViewPager.getCurrentItem());
            }
            catch(Exception e)
            {

            }
    }


    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mMainPagerAdapter.getItem(2).onActivityResult(requestCode,resultCode,data);
        if(mMainPagerAdapter.isTwitterLoggedIn())
        {
            try {
                mMainPagerAdapter.notifyDataSetChanged();
                mViewPager.setCurrentItem(mViewPager.getCurrentItem());
            }
            catch(Exception e)
            {

            }

        }
    }
}
