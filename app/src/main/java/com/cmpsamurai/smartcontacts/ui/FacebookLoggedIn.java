package com.cmpsamurai.smartcontacts.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.FacebookFriend;
import com.cmpsamurai.smartcontacts.util.FacebookContactListAdapter;
import com.cmpsamurai.smartcontacts.util.FacebookFriends;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FacebookLoggedIn#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FacebookLoggedIn extends Fragment {
    LoginButton loginButton;
    CallbackManager callbackManager;

    public FacebookLoggedIn() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters
     * @return A new instance of fragment FacebookLoggedIn.
     */
    // TODO: Rename and change types and number of parameters
    public static FacebookLoggedIn newInstance() {
        FacebookLoggedIn fragment = new FacebookLoggedIn();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_facebook_logged_in, container, false);
        ArrayList<FacebookFriend> fbFriends = FacebookFriends.getFacebookFriends();
        ListView listView = (ListView) view.findViewById(R.id.facebook_friends_listView);
        listView.setAdapter(FacebookFriends.getAdapter(getActivity()));

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
