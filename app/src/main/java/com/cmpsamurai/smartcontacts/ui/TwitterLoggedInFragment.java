package com.cmpsamurai.smartcontacts.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cmpsamurai.smartcontacts.R;
import com.cmpsamurai.smartcontacts.models.TwitterFollower;
import com.cmpsamurai.smartcontacts.util.TwitterFollowers;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TwitterLoggedInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TwitterLoggedInFragment extends Fragment {
    public TwitterLoggedInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TwitterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TwitterLoggedInFragment newInstance() {
        TwitterLoggedInFragment fragment = new TwitterLoggedInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_twitter_logged_in, container, false);
        TwitterFollowers.refreshFollowersList();

        ArrayList<TwitterFollower> twitterFollowers = TwitterFollowers.getTwitterFollowers();
        ListView listView = (ListView) view.findViewById(R.id.twitter_friends_listView);
        listView.setAdapter(TwitterFollowers.getAdapter(getActivity()));
        return view;
    }


}
