package com.cmpsamurai.smartcontacts.ui;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Lenovo-Laptop on 4/22/2016.
 */
public class ContactPagerAdapter  extends FragmentPagerAdapter {

    private Uri contactUri;

    public static final String details_tab_title="Contact Details";
    public static final String facebook_tab_title="Facebook Account";

    public ContactPagerAdapter(FragmentManager fm,Uri contactUri) {
        super(fm);
        this.contactUri=contactUri;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0: return ContactDetailFragment.newInstance(contactUri);
            default:return null;
        }
    }



    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position)
        {
            case 0:
                return details_tab_title;
        }
        return "Undefined";
    }

}
