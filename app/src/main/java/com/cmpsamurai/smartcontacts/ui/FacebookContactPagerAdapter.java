package com.cmpsamurai.smartcontacts.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cmpsamurai.smartcontacts.models.FacebookFriend;

/**
 * Created by Lenovo-Laptop on 5/17/2016.
 */
public class FacebookContactPagerAdapter extends FragmentStatePagerAdapter {
    public final String friend_detail_tab_title="Basic Information";
    public final String friend_posts_tab_title="User Updates";


    private FacebookFriend ff;
    public void setFacebookFriend(FacebookFriend ff){this.ff = ff;}

    public FacebookContactPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0 : return FacebookContactDetails.newInstance(ff);
            case 1 : return FacebookContactPosts.newInstance(ff);
            default : return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position)
        {
            case 0 : return friend_detail_tab_title;
            case 1 : return friend_posts_tab_title;
            default: return "Object" + + (position + 1);
        }

    }

    @Override
    public int getItemPosition(Object object)
    {
        return POSITION_UNCHANGED;
    }
}
